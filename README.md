# rpm-player

Play with RPM. In native Common Lisp.

Read. Write. Slurp.

```lisp
(with-open-rpm ("some.rpm" rpm archive)
  (let* ((header (header rpm))
         (index-entries (index-entries (header rpm)))
		 (index-entry (gethash 1006 index-entries)))
    (subseq (store header)
		    (header-index-entry-offset index-entry)
		    (+ 4 (header-index-entry-offset index-entry)))))
```

Very specific. Source: http://ftp.rpm.org/max-rpm/s1-rpm-file-format-rpm-file-format.html

MIT License.
