(uiop:define-package :rpm-player/rpm
    (:use :cl)
  (:import-from :lisp-binary
		#:defbinary
		#:magic
		#:fixed-length-string
		#:read-binary
		#:bad-magic-value)
  (:export :with-open-rpm

	   ;; rpm fields
	   :lead
	   :signature
	   :header

	   ;; header-index fields
	   :index-entries
	   :store))

(in-package :rpm-player/rpm)

(defbinary lead (:byte-order :big-endian)
  (m1 #xed :type (magic :actual-type (unsigned-byte 8)
			:value #xed))
  (m2 #xab :type (magic :actual-type (unsigned-byte 8)
			:value #xab))
  (m3 #xee :type (magic :actual-type (unsigned-byte 8)
			:value #xee))
  (m4 #xdb :type (magic :actual-type (unsigned-byte 8)
			:value #xdb))

  (maj 3 :type (unsigned-byte 8))
  (min 1 :type (unsigned-byte 8))

  (type #xff :type (unsigned-byte 16))
  (arch 0 :type (unsigned-byte 16))

  (name "" :type (fixed-length-string 66))

  (os 0 :type (unsigned-byte 16))
  (signature-version 5 :type (unsigned-byte 16))

  (reserved 0 :type (unsigned-byte 128)))

(defbinary header-header (:byte-order :big-endian)
  (magic #x8eade8 :type (magic :actual-type (unsigned-byte 24)
			       :value #x8eade8))
  (version-number 0 :type (unsigned-byte 8))
  (reserved 0 :type (unsigned-byte 32))

  (entries-count 0 :type (unsigned-byte 32))
  (data-length 0 :type (unsigned-byte 32)))

(defbinary header-index-entry (:byte-order :big-endian)
  (tag 0 :type (unsigned-byte 32))
  (type 0 :type (unsigned-byte 32))
  (offset 0 :type (unsigned-byte 32))
  (count 0 :type (unsigned-byte 32)))

(defclass header-entry ()
  ((index-entries :initarg :index-entries :reader index-entries)
   (store :initarg :store :reader store)))

(defun 8-byte-pad (number)
  (+ number (- 8 (mod number 8))))

(defun read-header (stream)
  (let* ((header-header (read-binary 'header-header stream))
	 (header-index-entries (make-hash-table)))
    (loop for i from 1 to (header-header-entries-count header-header)
       do (let ((header-index-entry (read-binary 'header-index-entry stream)))
	    (setf (gethash (header-index-entry-tag header-index-entry)
			   header-index-entries)
		  header-index-entry)))
    (make-instance
     'header-entry
     :index-entries header-index-entries
     :store (let* ((store-size (8-byte-pad (header-header-data-length header-header)))
		   (buffer (make-array store-size :element-type '(unsigned-byte 8))))
	      (read-sequence buffer stream)
	      buffer))))

(defclass rpm ()
  ((lead :initarg :lead :reader lead)
   (signature :initarg :signature :reader signature)
   (header :initarg :header :reader header)))

(defmacro with-open-rpm ((path rpm archive) &body body)
  (let ((stream (gensym)))
    `(with-open-file (,stream ,path :element-type '(unsigned-byte 8))
       (let ((,rpm (make-instance
		    'rpm
		    :lead (read-binary 'lead ,stream)
		    :signature (read-header ,stream)
		    :header (read-header ,stream)))
	     (,archive ,stream))
	 ,@body))))
