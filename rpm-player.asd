(defsystem "rpm-player"
  :license "MIT"
  :defsystem-depends-on ("wild-package-inferred-system")
  :class "winfer:wild-package-inferred-system"
  :depends-on ("rpm-player/*"))
